# OpenML dataset: coronary_dataset

https://www.openml.org/d/43154

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset description**

Probable risk factors for coronary thrombosis, comprising data from 1841 men.

**Format of the dataset**

The coronary data set contains the following 6 variables:

Smoking (smoking): a two-level factor with levels no and yes.

M. Work (strenuous mental work): a two-level factor with levels no and yes.

P. Work (strenuous physical work): a two-level factor with levels no and yes.

Pressure (systolic blood pressure): a two-level factor with levels <140 and >140.

Proteins (ratio of beta and alpha lipoproteins): a two-level factor with levels <3 and >3.

Family (family anamnesis of coronary heart disease): a two-level factor with levels neg and pos.

**Source** 

Edwards DI (2000). Introduction to Graphical Modelling. Springer, 2nd edition.

Reinis Z, Pokorny J, Basika V, Tiserova J, Gorican K, Horakova D, Stuchlikova E, Havranek T, Hrabovsky F (1981). "Prognostic Significance of the Risk Profile in the Prevention of Coronary Heart Disease". Bratisl Lek Listy, 76:137-150. Published on Bratislava Medical Journal, in Czech.

Whittaker J (1990). Graphical Models in Applied Multivariate Statistics. Wiley.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43154) of an [OpenML dataset](https://www.openml.org/d/43154). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43154/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43154/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43154/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

